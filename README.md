# Batch effect adjustment through piece-wise linear transformations (BEATLE) <img src="man/figures/logo.png" align="right" width="120" />

This repository provides an R package for batch effect adjustment in
case-control microbiome datasets through piece-wise linear transformations.

Technical variations or batch effect can hinder the interpretation of
high-throughput datasets. This package provides a method to correct batch
effects in case-control microbiome datasets.  
For all microbial features that differ between the controls in both datasets, 
a piecewise-linear transformation function is learned based on the quantiles of 
the relative abundance distribution in the reference dataset. 
This function is used for quantile normalization [[1]](#1) in the target dataset (datasets of varying numbers of samples). 
The relative abundances in the target dataset are then adjusted using this transformation function. 
Importantly, this approach uses monotonic (rank-preserving) transformation and preserves sparsity; 
it does not assign non-zero abundances to features that originally have zero abundance. 

<img src="man/figures/dragons.png" align='left' width="100"/>

__hic sunt dracones!__  
__The package is under on-going development and may not be very stable!!!__


## Installation

The package can be installed
- either via `devtools` (you will have to exchange `username` by your user name)
```R
require('devtools')
require('getPass')
require('git2r')
devtools::install_git(
    'https://git.embl.de/grp-zeller/batch_effect_correction.git',
    credentials = git2r::cred_user_pass("username", getPass::getPass())
)
```
- or from source
```bash
git clone https://git.embl.de/grp-zeller/batch_effect_correction.git
R CMD INSTALL batch_effect_correction
```

## Usage

Simply put, the package provides a single function (`correct.batch`), which
will learn a transformation and adjust the target dataset. The usage is pretty
straightforward:
```R
library("BEATLE")

res <- correct.batch(target.data,           # Target dataset to be transformed
                     reference.data,        # Reference dataset
                     log.n0=1e-06,          # Pseudo-count to be added
                     ref.ctr.idx=NULL,      # Indices of control instances in the reference dataset
                     trg.ctr.idx=NULL,      # Indices of control instances in the target dataset
                     diagnostic.plot=NULL,  # Filename of diagnostic plot
                     verbose=1)
```

> __Please note!__  
    The function expects relative abundances!


## Contact

For questions, suggestions, or in the case of any problems, please feel free
to contact me, [Jakob Wirbel](mailto:jakob.wirbel@embl.de).

## References
<a id="1">[1]</a> Bolstad, B. M., Irizarry, R. A., Åstrand, M., & Speed, T. P. (2003). A comparison of normalization methods for high density oligonucleotide array data based on variance and bias. Bioinformatics, 19, 185–193. [PubMed](https://pubmed.ncbi.nlm.nih.gov/12538238/)
