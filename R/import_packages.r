#!/usr/bin/Rscript
### batch.effect.correction - Package for batch effect correction of
###   case-control microbiome datasets
### EMBL Heidelberg 2019 GNU GPL 3.0

#' @importFrom quadprog solve.QP
#' @importFrom progress progress_bar
#' @importFrom grDevices dev.off pdf
#' @importFrom graphics abline boxplot lines mtext par plot points
#' @importFrom stats quantile rnorm

NULL
